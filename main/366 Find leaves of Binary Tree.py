"""
# 366 Find Leaves of Binary Tree

[https://leetcode.com/problems/find-leaves-of-binary-tree/](https://leetcode.com/problems/find-leaves-of-binary-tree/)

[https://www.lintcode.com/problem/650/](https://www.lintcode.com/problem/650/)

Given the `root` of a binary tree, collect a tree's nodes as if you were doing this:

- Collect all the leaf nodes.
- Remove all the leaf nodes.
- Repeat until the tree is empty.

**Example 1:**

![https://assets.leetcode.com/uploads/2021/03/16/remleaves-tree.jpg](https://assets.leetcode.com/uploads/2021/03/16/remleaves-tree.jpg)

```
Input: root = [1,2,3,4,5]
Output: [[4,5,3],[2],[1]]
Explanation:
[[3,5,4],[2],[1]] and [[3,4,5],[2],[1]] are also considered correct answers since per each level it does not matter the order on which elements are returned.

```

**Example 2:**

```
Input: root = [1]
Output: [[1]]

```

**Constraints:**

- The number of nodes in the tree is in the range `[1, 100]`.
- `100 <= Node.val <= 100`
"""

A=[]; n=0
def leavesOfBinaryTree(root):
    if not root:
        return -1
    # if root.left==root.right==None:
    #     A[0].append(root)
    #     return 0
    l=leavesOfBinaryTree(root.left)
    r=leavesOfBinaryTree(root.right)
    A[max(l,r)+1].append(root)
    return max(l,r)+1