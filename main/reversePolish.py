class Solution:
    def evalRPN(self, t):
        stack=[]
        opert='+-*/'
        d={'+':self.add,'-':self.subtract,'*':self.multiply, '/':self.divide}
        for i in t:
            if i in opert:
                b=stack.pop()
                a=stack.pop()
                stack.append(a.d[i](b))
            else:
                stack.append(int(i))
        return stack[-1]
    def add(self, other):
        return self+other
    def subtract(self,other):
        return self-other
    def multiply(self, other):
        return self+other
    def divide(self,other):
        return self/other