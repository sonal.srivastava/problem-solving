import queue


def addValidMoves(grid, BFSqueue, xCoord, yCoord, steps, obstacles, numRows, numCols, obstaclesAllowed):
                  #visited):
    validMoves = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    for x, y in validMoves:
        newXCoord = x + xCoord
        newYCoord = y + yCoord
        if -1 < newXCoord < numRows and -1 < newYCoord < numCols:
            newSteps = steps + 1
            newObstacles = obstacles
            if isinstance(grid[newXCoord][newYCoord], tuple):
            #if (newXCoord, newYCoord) in visited:  # If this point is already visited
            #    s, k = visited[(newXCoord, newYCoord)]
                s, k = grid[newXCoord][newYCoord]
                # If steps stored for the point is more than what this path can achieve, then we should add this new point in PQ
                if s > newSteps:
                    pass
                else:
                    continue

            if grid[newXCoord][newYCoord] == 1:  # obstacle
                #print("obstacle found, ", newXCoord, newYCoord, newObstacles)
                newObstacles += 1
                if newObstacles > obstaclesAllowed:
                    continue
                else:
                    BFSqueue.put((newSteps, newObstacles, newXCoord, newYCoord))
                    #heapq.heappush(priorityQueue, (newSteps, newObstacles, newXCoord, newYCoord))
            else:  # no obstacle
                BFSqueue.put((newSteps, newObstacles, newXCoord, newYCoord))
                #heapq.heappush(priorityQueue, (newSteps, newObstacles, newXCoord, newYCoord))




class Solution:
    def shortestPath(self, grid, obstaclesAllowed):
        numRows = len(grid)
        numCols = len(grid[0])
        #priorityQueue = [(0, 0, 0, 0)]  # steps, obstacles, x, y
        #heapq.heapify(priorityQueue)
        BFSqueue=queue.Queue()
        BFSqueue.put((0, 0, 0, 0))
        #visited = dict()#{(0, 0): (0, 0)}
        while not BFSqueue.empty():
            minimumStepsPointExtracted = BFSqueue.get()#heapq.heappop(priorityQueue)
            steps, obstacles, xCoord, yCoord = minimumStepsPointExtracted
            #visited[(xCoord, yCoord)] = (steps, obstacles)
            grid[xCoord][yCoord]=(steps, obstacles)
            #print(xCoord, yCoord, steps, obstacles, "  VISITED=>  ", visited, "PRIORITY Q",priorityQueue)
            if xCoord==numRows-1 and yCoord==numCols-1:
                return steps
            addValidMoves(grid, BFSqueue, xCoord, yCoord, steps, obstacles, numRows, numCols, obstaclesAllowed)
                          #visited)
        return -1



obj = Solution()
print(obj.shortestPath([[0, 0, 0], [1, 1, 0], [0, 0, 0], [0, 1, 1], [0, 0, 0]], 1))

print(obj.shortestPath([[0,1,1],[1,1,1],[1,0,0]], 1))
