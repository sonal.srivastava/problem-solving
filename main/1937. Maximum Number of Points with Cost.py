class Solution:
    def maxPoints(self, pointMatrix):
        numberOfColumns = len(pointMatrix[0])

        previousRow = [0] * numberOfColumns
        for row in pointMatrix:
            for j in range(1, numberOfColumns):
                previousRow[j] = max(previousRow[j], previousRow[j - 1] - 1)
            for j in range(numberOfColumns - 2, -1, -1):
                previousRow[j] = max(previousRow[j], previousRow[j + 1] - 1)
            currentRow = [x + y for x, y in zip(row, previousRow)]
            previousRow = currentRow
        return max(previousRow)


obj = Solution()
print(obj.maxPoints([[3, 9, 7, 8], [100, 20, 1, 60], [2, 5, 8, 11], [3, 3, 96, 3]]))
